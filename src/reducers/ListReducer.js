import { CONSTANTS } from "../actions";

let listId = 3;
let cardId = 6;
const initialState = [
  {
    title: "Yesterday",
    id: `list-${0}`,
    cards: [
      {
        id: `card-${0}`,
        text: "we created static cards"
      },
      {
        id: `card-${1}`,
        text: "we created dynamic cards"
      }
    ]
  },
  {
    title: "Today",
    id: `list-${1}`,
    cards: [
      {
        id: `card-${2}`,
        text: "we created static cards"
      },
      {
        id: `card-${3}`,
        text: "we created dynamic cards"
      },
      {
        id: `card-${4}`,
        text: "we created cards"
      }
    ]
  },
  {
    title: "Tomorrow",
    id: `list-${2}`,
    cards: [
      {
        id: 0,
        text: "we created static cards"
      },
      {
        id: `card-${5}`,
        text: "we created dynamic cards"
      }
    ]
  }
];

const ListReducer = (state = initialState, action) => {
  switch (action.type) {
    case CONSTANTS.ADD_LIST:
      const newList = {
        title: action.payload,
        cards: [],
        id: `list-${listId}`
      };
      listId += 1;
      return [...state, newList];

    case CONSTANTS.ADD_CARD: {
      const newCard = {
        text: action.payload.text,
        id: `list-${cardId}`
      };
      cardId += 1;
      const newState = state.map(list => {
        if (list.id === action.payload.listId) {
          return {
            ...list,
            cards: [...list.cards, newCard]
          };
        } else {
          return list;
        }
      });
      return newState;
    }

    case CONSTANTS.DRAG_HAPPENED:
      const {
        droppableIdStart,
        droppableIdEnd,
        droppableIndexStart,
        droppableIndexEnd,
        draggableId
      } = action.payload;

      const newState = [...state];

      //in the same list
      if (droppableIdStart === droppableIdEnd) {
        const list = state.find(list => droppableIdStart === list.id);
        const card = list.cards.splice(droppableIndexStart, 1);
        list.cards.splice(droppableIndexEnd, 0, ...card);
      }

      //other list
      if (droppableIdStart !== droppableIdEnd) {
        //find list where drag happened
        const listStart = state.find(list => droppableIdStart === list.id);

        //pullout card from this list
        const card = listStart.cards.splice(droppableIndexStart, 1);

        //find list where drag ended
        const listEnd = state.find(list => droppableIdEnd === list.id);
        //pull card in new list
        listEnd.cards.splice(droppableIndexEnd, 0, ...card);
      }

      return newState;

    default:
      return state;
  }
};

export default ListReducer;
