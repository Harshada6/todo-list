import React from "react";
import TrelloCard from "./TrelloCards";
import TrelloActionButton from "./TrelloActionButton";
import { Droppable } from "react-beautiful-dnd";
import styled from "styled-components";

const ListContainer = styled.div`
  background-color: #dcdcdc;
  border-radius: 3px;
  width: 300px;
  padding: 8px;
  margin-right: 8px;
  height: 100%;
`;

const TrelloList = ({ title, cards, listId }) => {
  return (
    <Droppable droppableId={String(listId)}>
      {provided => (
        <ListContainer {...provided.droppableProps} ref={provided.innerRef}>
          <h1>{title}</h1>
          {cards.map((card, index) => (
            <TrelloCard
              key={card.id}
              index={index}
              text={card.text}
              id={card.id}
            />
          ))}
          <TrelloActionButton listId={listId} />
          {provided.placeholder}
        </ListContainer>
      )}
    </Droppable>
  );
};

// const styles = {
//   container: {
//     backgroundColor: "#DCDCDC",
//     borderRadius: 3,
//     width: 300,
//     padding: 8,
//     marginRight: 8,
//     height: "100%"
//   }
// };

export default TrelloList;
